# Create VPC and EKS Cluster Using-Terrafrom-GITLABCI

## To create a cluster use these Variables to run pipeline

> BUILD_CLUSTER = true

## To install ADDONS for EKS Cluster other than kube-proxy, Coredns and VPC-CNI 

> INSTALL_ADDONS = true

### It will install the Below Addons

* METRICS-SERVER
* AWSLBcontroller
* AWSClusterAutoscaler
* ARGOCD

## To Uninstall ADDONS and CLuster use below variable to run Pipeline

> REMOVE_ALL = true

## To destroy only the Cluster

> DESTROY_CLUSTER = true

**Its a manual action as its Destructive**