terraform {
  required_version = "~> 1.7.3"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.36.0"
    }
  }
  backend "s3" {
    bucket = "sidterraformstates-1"
    key    = "final/terraform.tfstate"
    region = "ap-south-1"
  }
}


provider "aws" {
  region = "us-east-1"
}

